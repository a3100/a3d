import Vue from 'vue';
import Vuex from 'vuex';
import claimsApi from '../api/claims'

Vue.use(Vuex)
const axios = require('axios');
export default new Vuex.Store({
    state: {
        claims: frontendData.claims,
        profile: frontendData.profile,
        listAdmins: frontendData.listAdmins,
        profileClaims: frontendData.profileClaims,
        claimSubscriber: frontendData.claimSubscriber,
        vacanciesProfile: frontendData.vacanciesProfile
    },
    getters: {
        sortedClaims: state => state.claims.sort((a, b) => b.id - a.id),
        listAdmins: state => state.listAdmins,
        profileClaims: state => state.profileClaims,
        claimSubscriber: state => state.claimSubscriber,
        vacanciesProfile: state => state.vacanciesProfile
    },
    mutations: {
        addClaimMutation(state, claim) {
            state.claims = [
                ...state.claims,
                claim
            ]
        },
        updateClaimMutation(state, claim) {
            const updateIndex = state.claims.findIndex(item => item.id === claim.id);
            state.claims = [
                ...state.claims.slice(0, updateIndex),
                claim,
                ...state.claims.slice(updateIndex + 1)
            ]
        },
        removeClaimMutation(state, claim) {
            const deletionIndex = state.claims.findIndex(item => item.id === claim.id);
            if (deletionIndex > -1) {
                state.claims = [
                    ...state.claims.slice(0, deletionIndex),
                    ...state.claims.slice(deletionIndex + 1)
                ]
            }
        }
    },
    actions: {
        async addClaimAction({commit, state}, claim) {
            console.log(claim);
            let files = claim.files;
            claim.files = [];
            claimsApi.add(claim).then(result => {
                let formDatas = [];
                for (let i = 0; i < files.length; i++) {
                    let formData = new FormData();
                    formData.append('file', files[i].file);
                    formData.append('fileName', files[i].file.name);
                    formData.append('numberOfCopy', files[i].numbersOfCopy);
                    formData.append('claimId', result.data.id);
                    formDatas.push(formData);
                }

                formDatas.forEach(function (formData, index) {
                    setTimeout(function () {
                            axios.default.post('/claim/single-file',
                                formData,
                                {
                                    headers: {
                                        'Content-Type': 'multipart/form-data'
                                    }
                                }
                            );
                            index++;
                        },
                        1000 * index);
                });


                console.log(result);
            }, reason => {
                console.log(reason);
            });

        },
        async updateClaimAction({commit}, claim) {
            const result = await claimsApi.update(claim)
            const data = await result.json()
            commit('updateClaimMutation', data)
        },
        async removeClaimAction({commit}, claim) {
            const result = await claimsApi.remove(claim.id)

            if (result.ok) {
                commit('removeClaimMutation', claim)
            }
        },
    }
})
