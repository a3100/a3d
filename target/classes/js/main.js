import Vue from 'vue'
import Vuetify from "vuetify";
import '@babel/polyfill'
import '../js/api/resource'
import store from '../js/store/store'
import router from '../js/router/router'
import App from './pages/App.vue'
import { connect } from "util/ws";
import vb from 'vue-babylonjs';
import { plugin, Scene, Box, Cannon } from 'vue-babylonjs';
Vue.use(plugin, { components: { Scene, Box, Physics: Cannon } });

import 'vuetify/dist/vuetify.min.css'

if (frontendData.profile) {
    connect()
}
Vue.use(Vuetify);
Vue.use(vb);
new Vue({
    el: '#app',
    store,
    router,
    render: a => a(App),
    vuetify: new Vuetify({ icons: { iconfont: 'md' } }),
});
