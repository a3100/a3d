import Vue from 'vue'

const claims = Vue.resource('/claim{/id}')
export default {
    add: claim => claims.save({}, claim),
    update: claim => claims.update({id: claim.id}, claim),
    remove: id => claims.remove({id})
}
