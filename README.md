# Руководство разработчика Web-сервиса

## Back-end

### Структура проекта
Для построения backend исполоьзовался [Spring](https://spring.io/projects/spring-framework) в связке с  [Spring Boot](https://spring.io/projects/spring-boot). Подробно про фреймворк можно почитать [здесь](https://spring.io).

В корневой папке находится `pom.xml`, хранящий в себе все зависимости приложения. `application.properties` - файл для настроек находится по пути - `src/main/resources`.

Все, необходимое для разработки, лежит в пакете `src/main/auto3dtrackingsystem`. Данный пакет включает в себя 7 пакетов:

#### config

- `WebMvcConfig` - файл с конфигурацией [SpringWebMvc](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc)
- `WebSecurityConfig` - файл с конфигурацией [SpringSecurity](https://spring.io/projects/spring-security-oauth). Авторизация в приложении происходит с помощью OAuth2.0.
- `WebSocketConfig` - файл с конфигурацией [SpringSocket](https://spring-projects.ru/guides/messaging-stomp-websocket/)
- `SpringFoxConfig` - файл для настрочки [Swagger](https://swagger.io)
- `MailConfig` - файл для настрочки [SpringEmail](https://www.baeldung.com/spring-email)
 


#### domain

Структура основных классов программы: 
![UML](/uploads/24df4b16065fb70b12ec8169398583c5/UML.png)

#### controllers

API программы отображаются в [Swaggere](https://swagger.io) или их можно посмотреть: 
[api-documentation.pdf](/uploads/624dd8e6d4bde7e226a7f476dbb75bb2/api-documentation.pdf) и [api-documentation.yaml](/uploads/01efb60a6dbb72e973f1d9effb4b5060/swagger.yaml)


#### repository

Мы используем [Spring Data](https://spring.io/projects/spring-data) для общения с Базой данных поэтому в данном пакете лежат интерфейсы репозиториев `ClaimRepository` и `UserDetailsRepository`

#### dto

В данном пакете лежат dto файлы для общения с клиентом: `EventType`,`ObjectType`,`WsEventDto`.

#### service

Пакет service предназначен почти для всей логики проекта. В нем находятся:
- `ExcelService` - сервис формирования Excel документов на основании информации в заявках. Построен на основе библиотеки [ApachePoi](https://poi.apache.org)
- `MailSender` - настройка и формирование сообщений, формирование Бина [JavaMailSender](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/mail/javamail/JavaMailSender.html)
- `StatusListener` - лиснер, предооставляющий внутреннюю логику отправки сообщений.
- `UserService` - сервис, который предоставляет взаимодействие с пользователем.

## Front-end

### Структура проекта
Данная часть проекта написана на framework - `Vue.js`, использовав для вёрстки сервиса [Vuetify.js](https://vuetifyjs.com/ru/). Подробно про фреймворк можно почитать [здесь](https://vuejs.org/).

Все, необходимое для разработки, лежит в пакете `src/main/resources/static/js`. Данный пакет включает в себя 6 пакетов:

#### api

В данном пакете содержится файл `claims.js`, реализация которого предназначена на удаление, добавление, обновление заявки для любого компонента.

#### router

В данном пакете содержится файл `router.js`, который осуществляет навигацию между основными вкладками: 
- `/createClaim` - страница создания заявки
- `/claimList` - страница заявок пользователя
- `/instruction` - страница с информацией по подаче заявки
- `/changeStatus` - страница с панелью администратора

#### components/messages

Данный пакет является одним из основных и содержит вёрстку основных вкладок на UI, работу с API и 3D-моделью. Содержит в себе 3 "Components":

- `ClaimForm.vue` - форма для подачи заявки
- `Instruction.vue (/instruction)` - вкладка с детальной информацией по подаче заявки
- `TableListClaims.vue (/changeStatus)` - панель администратора (просмотр 3D-модели, назначение проверяющих к заявке, скачиваение отчётности в Excel, изменение статуса готовности заявок, фильтрация таблицы по ключевым полям)

`TableListClaims.vue` работает на основе компонентов `<v-data-table>, <v-dialog>, <v-container>`. 
Как работать с этими элементами, подробно разобрано в этих статьях: [v-data-table](https://vuetifyjs.com/en/components/data-tables/) , [v-dialog](https://vuetifyjs.com/ru/components/dialogs/) , [v-container](https://vuetifyjs.com/ru/components/grids/).
 - `<v-data-table>` - формирует таблицу данных, где хранятся все поданные заявки всех пользователей
 - `<v-dialog>` - создаёт диалоговые окна для назначения проверяющих к заявки, а также для просмотра 3D-модели
 - `<v-container>` - структурирует кнопки, фильтры на UI

#### pages

Данный пакет явлеется одним из основных и содержит панель смены интерфейсов, а также методы, реализующие компоненты в пакете `components/messages`. Содержит в себе 3 "Components" :

- `ClaimsList.vue (/claimList)` - выводит таблицу со всеми заявками данного пользователя (реализовано на основе компонента `TableListClaims.vue`)
- `CreateClaim (/createClaim)` - выводит форму подачи заявки, а также сохраняет все данные заявки в БД
- `App.vue` - <toolbar> навигация, для смены вкладок (количество вкладок определяется в зависимости от того, кто зашёл: пользователь или администратор)

`App.vue` работает на основе компонентов `<v-toolbar>, <v-dialog>`. Как подробно работать с компонентом "v-toolbar" можно почитать [тут](https://vuetifyjs.com/en/components/toolbars/).
- `<v-dialog>` - создаёт диалоговое окно для добавления, удаления администратора
- `<v-toolbar>` - здесь расположены вкладки, каждая из которых содержит определённый API для перехода на соответствующую страницу, учитывая роль.

#### store

В данном пакете содержится файл `store.js`, который напрямую работает с back-end. При старте сервиса, функционал этого файла вытаскивает из сервиса определённые значения и инициализирует их на front-end части.

#### util

В данном пакете содержится файл `ws.js`, который содержит настройку `web-socket`.

### Работа с сетью

Запросы к серверу выполнялись с помощью библиотеки `$http`.

#### API

Здесь кратко указано назначение каждого API:

- `/auth` - авторизация на сервере, куда под капотом отправляется google token
- `/claim/create` - создание заявки, в качестве параметров передаются все, заполненные пользователем, поля
- `/claim/single-file` - загрузка файла модели на сервер, привязан к id заявки, который мы получаем в `/claim/create`
- `/claim/list/` - получение заявок для конкретного пользователя, в качестве параметра передается id пользователя
- `/claim` - получение списка всех заявок, вызывается в администраторской версии
- `/claim/user/role/` - получение уровня доступа пользователя (админ или обычный пользователь), на основе которого определяется сколько заявок получить
- `/claim/file` - получение модели с сервера для просмотра, доступно администратору
- `/claim/statusChange` - смена статуса заявки, доступно только администратору, в качесвте параметра передается новый статус заявки

# Руководство пользователя Web-сервиса





