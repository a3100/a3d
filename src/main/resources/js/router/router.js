import Vue from 'vue'
import VueRouter from 'vue-router'
import CreateClaim from '../pages/CreateClaim.vue'
import ClaimsList from "../pages/ClaimList.vue";
import myClaimsList from "../pages/ClaimsList.vue";
import TableListClaims from "../messages/TableListClaims.vue";
import AdminRow from '../pages/AdminRow.vue';
import Instruction from "../messages/Instruction.vue";
import MainPage from '../pages/MainPage.vue';

Vue.use(VueRouter)

const routes = [
    { path: '/createClaim', component: CreateClaim, props: true },
    { path: '/addAdmin', component: AdminRow },
    { path: '/claimList', component: ClaimsList },
    { path: '/myClaimList', component: myClaimsList },
    { path: '/changeStatus', component: TableListClaims },
    { path: '/instruction', component: Instruction },
    { path: '/', component: MainPage }
];

export default new VueRouter({
    mode: 'history',
    routes
})
