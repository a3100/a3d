package auto3dtrackingsystem.domain;

import auto3dtrackingsystem.service.StatusListener;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table
@ToString(of = {"id", "description"})
@EqualsAndHashCode(of = {"id"})
@Getter
@Setter
@EntityListeners(StatusListener.class)
public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(Views.FullClaims.class)
    private Long id;

    @Column(updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonView(Views.FullClaims.class)
    private LocalDateTime receiptDate;
    @JsonView(Views.FullClaims.class)
    @Enumerated(EnumType.ORDINAL)
    private Status status = Status.CREATED;
    @Column(updatable = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonView(Views.FullClaims.class)
    private LocalDate leadTime;
    @JsonView(Views.FullClaims.class)
    private int lineNumber;
    @JsonView(Views.FullClaims.class)
    private LocalDate orderCompletionDate;
    @JsonView(Views.FullClaims.class)
    private int cost;
    @JsonView(Views.FullClaims.class)
    private int resourceProvision;
    @JsonView(Views.FullClaims.class)
    private String fio;
    @JsonView(Views.FullClaims.class)
    private String op;
    @JsonView(Views.FullClaims.class)
    private int course;
    @JsonView(Views.FullClaims.class)
    private String projectName;
    @JsonView(Views.FullClaims.class)
    private String scientificDirector;
    @JsonView(Views.FullClaims.class)
    private String contactNumber;
    @JsonView(Views.FullClaims.class)
    private String contactEmail;
    @JsonView(Views.FullClaims.class)
    private String nameCompany;
    @JsonView(Views.FullClaims.class)
    private String requirements;

    @JsonView(Views.FullClaims.class)
    @ManyToOne
    private User user;

    @ManyToMany
    private List<User> subscriber;

    @JsonIgnore
    @Lob
    @Column(length=100000)
    private byte[] file;

    @JsonView(Views.FullClaims.class)
    private String fileName;

    @JsonView(Views.FullClaims.class)
    private Integer numberOfCopy;

    @JsonView(Views.FullClaims.class)
    private String description;

    @ManyToOne
    private User checker;


}
