package auto3dtrackingsystem.domain;

public enum Status {
    CREATED, IN_WORK, COMPLETED, CANCELED
}
