package auto3dtrackingsystem.domain;

import auto3dtrackingsystem.service.StatusListener;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "vacancies")
@Data
@EqualsAndHashCode(of = { "id" })
@EntityListeners(StatusListener.class)
public class Vacancies {

    @Id
    private String Id;
    private String claimId;
    private String userId;
    private String nameCompany;
    private String emailCompany;
    private String description;
    private String emailSubscriber;
    private String createrClaimId;
    private String chance;

    @JsonView(Views.FullClaims.class)
    @Enumerated(EnumType.ORDINAL)
    private Status status = Status.CREATED;

}
