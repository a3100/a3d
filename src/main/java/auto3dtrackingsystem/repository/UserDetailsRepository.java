package auto3dtrackingsystem.repository;

import auto3dtrackingsystem.domain.Role;
import auto3dtrackingsystem.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDetailsRepository extends JpaRepository<User, String> {

    User findByEmail(String email);
    List<User> findByRoles(Role roles);
}
