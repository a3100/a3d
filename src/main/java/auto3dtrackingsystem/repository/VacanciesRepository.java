package auto3dtrackingsystem.repository;

import auto3dtrackingsystem.domain.Vacancies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacanciesRepository extends JpaRepository<Vacancies, String> {

    String findByUserId(String user_id);
}
