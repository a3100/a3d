package auto3dtrackingsystem.service;

import auto3dtrackingsystem.domain.Claim;
import auto3dtrackingsystem.domain.Status;
import auto3dtrackingsystem.domain.User;
import auto3dtrackingsystem.domain.Vacancies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.PostUpdate;

@Component
public class StatusListener {

    @Autowired
    private MailSender mailSender;

    @PostUpdate
    public void setUpdatedOn(Vacancies claim) {
        if (claim.getStatus() == Status.CREATED) {
            String message = String.format(
                    "Добрый день, %s! " +
                            "Ваша заявка СОЗДАНА. В ближайшее время с вами свяжется сотрудник данной компании для дальнейших действий."
            );
            mailSender.send(claim.getEmailSubscriber(), "Update Status your claim", message);
        }
        if (claim.getStatus() == Status.COMPLETED) {
            String message =
                    "Добрый день! " +
                            "Ваша заявка ПРИНЯТА. В ближайшее время с вами свяжется сотрудник данной компании для дальнейших действий."
            ;
            mailSender.send(claim.getEmailSubscriber(), "Update Status your claim", message);
        }
        if (claim.getStatus() == Status.IN_WORK) {
            //User user = claim.getUser();
            String message = String.format(
                    "Добрый день, %s! " +
                            "Ваша заявка В РАБОТЕ. Все подробности можете узнать в личном кабинете."// user.getName()
            );
            mailSender.send(claim.getEmailSubscriber(), "Update Status your claim", message);
        }
        if (claim.getStatus() == Status.CANCELED) {
            //User user = claim.getUser();
            String message = String.format(
                    "Добрый день, %s! " +
                            "К сожалению, Ваша заявка отклонена."// user.getName()
            );
            mailSender.send(claim.getEmailSubscriber(), "Update Status your claim", message);
        }
    }


}
