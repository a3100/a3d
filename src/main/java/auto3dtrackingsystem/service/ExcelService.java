package auto3dtrackingsystem.service;

import auto3dtrackingsystem.domain.Claim;
import auto3dtrackingsystem.repository.ClaimRepository;
import org.apache.http.client.utils.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExcelService {
    @Autowired
    private final ClaimRepository claimRepository;

    public ExcelService(ClaimRepository claimRepository) {
        this.claimRepository = claimRepository;
    }

    public File excel() {
        List<Claim> claimList = claimRepository.findAll().stream()
                .sorted(Comparator.comparingLong(Claim::getId))
                .collect(Collectors.toList());
        File file = new File("./src/main/resources/excel.xls");
        try {
            writeIntoExcel(file.getAbsolutePath(), claimList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }


    public void writeIntoExcel(String file, List<Claim> claims) throws FileNotFoundException, IOException {
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet("Заявки на печать");
        Row top = sheet.createRow(0);
        Cell cell = top.createCell(0);
        cell.setCellValue("Отчёт по заявкам на 3д печать");
        sheet.createRow(1);
        Row header = sheet.createRow(2);

        Cell id = header.createCell(0);
        id.setCellValue("Ид");

        Cell status = header.createCell(1);
        status.setCellValue("Статус");

        Cell fio = header.createCell(2);
        fio.setCellValue("ФИО");

        Cell op = header.createCell(3);
        op.setCellValue("Образовательная программа");


        Cell contactEmail = header.createCell(11);
        contactEmail.setCellValue("Название проекта");

        Cell coerce = header.createCell(4);
        coerce.setCellValue("Курс");

        Cell projectName = header.createCell(5);
        projectName.setCellValue("Название проекта");


        Cell receiptDated = header.createCell(6);
        receiptDated.setCellValue("Дата создания");

        Cell checker = header.createCell(7);
        checker.setCellValue("Проверяющий");

        Cell fileName = header.createCell(8);
        fileName.setCellValue("Название файла");

        Cell numberOfCopy = header.createCell(9);
        numberOfCopy.setCellValue("Количество копий");

        Cell description = header.createCell(10);
        description.setCellValue("Описание");




        int index = 3;
        for (Claim claim : claims) {

            // Нумерация начинается с нуля
            Row row = sheet.createRow(index);

            // Мы запишем имя и дату в два столбца
            // имя будет String, а дата рождения --- Date,
            // формата dd.mm.yyyy
            Cell claimId = row.createCell(0);
            claimId.setCellValue(claim.getId().toString());

            Cell claimStatus = row.createCell(1);
            claimStatus.setCellValue(claim.getStatus().toString());

            Cell claimFio = row.createCell(2);
            claimFio.setCellValue(claim.getFio());

            Cell claimOp = row.createCell(3);
            claimOp.setCellValue(claim.getOp());

            Cell claimCoerce = row.createCell(4);
            claimCoerce.setCellValue(claim.getCourse() + "");

            Cell claimProjectName = row.createCell(5);
            claimProjectName.setCellValue(claim.getProjectName());

            Cell getReceiptDate = row.createCell(6);

            DataFormat format = book.createDataFormat();
            CellStyle dateStyle = book.createCellStyle();
            dateStyle.setDataFormat(format.getFormat("dd.MM.yyyy hh:mm"));
            getReceiptDate.setCellStyle(dateStyle);


            // Нумерация лет начинается с 1900-го
            getReceiptDate.setCellValue(Date.from(claim.getReceiptDate().atZone( ZoneId.systemDefault()).toInstant()));

            if (claim.getChecker() != null) {
                Cell getChecker = row.createCell(7);
                getChecker.setCellValue(claim.getChecker().getEmail());
            }


            Cell getFileName = row.createCell(8);
            getFileName.setCellValue(claim.getFileName());

            if (claim.getNumberOfCopy() != null) {
                Cell getNumberOfCopy = row.createCell(9);
                getNumberOfCopy.setCellValue(claim.getNumberOfCopy());
            }


            Cell getDescription = row.createCell(10);
            getDescription.setCellValue(claim.getDescription());

            Cell getContactEmail = row.createCell(11);
            getContactEmail.setCellValue(claim.getContactEmail());
            index++;
        }


        // Меняем размер столбца
        sheet.autoSizeColumn(1);

        // Записываем всё в файл
        book.write(new FileOutputStream(file));
        book.close();
    }
}
