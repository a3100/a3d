package auto3dtrackingsystem.controllers;

import auto3dtrackingsystem.domain.Claim;
import auto3dtrackingsystem.domain.Role;
import auto3dtrackingsystem.domain.User;
import auto3dtrackingsystem.domain.Vacancies;
import auto3dtrackingsystem.repository.ClaimRepository;
import auto3dtrackingsystem.repository.UserDetailsRepository;
import auto3dtrackingsystem.repository.VacanciesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/")
public class MainController {
    private final ClaimRepository claimRepository;
    private final UserDetailsRepository userDetailsRepository;

    @Value("${spring.profiles.active:prod}")
    private String profile;

    @Autowired
    private VacanciesRepository vacanciesRepository;

    @Autowired
    public MainController(ClaimRepository claimRepository, UserDetailsRepository userDetailsRepository) {
        this.claimRepository = claimRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    @GetMapping
    public String main(Model model, @AuthenticationPrincipal User user) {
        HashMap<Object, Object> data = new HashMap<>();
        List<Claim> profileClaims = new ArrayList<>();
        List<Vacancies> claimSubscriber = new ArrayList<>();
        List<Vacancies> vacanciesProfile = new ArrayList<>();
        User userProfile = null;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!principal.equals("anonymousUser")) {
             userProfile = (User) principal;
        }
        if (user != null) {
            data.put("profile", user);
            data.put("claims", claimRepository.findAll());
            data.put("listAdmins", userDetailsRepository.findByRoles(Role.ADMIN));
            if (userProfile != null) {
                for (Claim claim : claimRepository.findAll()) {
                    if (claim.getUser().getId().equals(userProfile.getId())) {
                        profileClaims.add(claim);
                    }
                }
                data.put("profileClaims", profileClaims);
            }

            if (userProfile != null) {
                for (Vacancies vacancies : vacanciesRepository.findAll()) {
                    if (vacancies.getUserId().equals(userProfile.getId())) {
                        claimSubscriber.add(vacancies);
                    }
                }
                data.put("claimSubscriber", claimSubscriber);
            }

            if (userProfile != null) {
                for (Vacancies vacancies : vacanciesRepository.findAll()) {
                    if (vacancies.getCreaterClaimId().equals(userProfile.getId())) {
                        vacanciesProfile.add(vacancies);
                    }
                }
                data.put("vacanciesProfile", vacanciesProfile);
            }
        }

        model.addAttribute("frontendData", data);
        model.addAttribute("isDevMode", "dev".equals(profile));

        return "index";
    }
}
