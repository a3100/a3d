package auto3dtrackingsystem.controllers;

import auto3dtrackingsystem.domain.*;
import auto3dtrackingsystem.dto.EventType;
import auto3dtrackingsystem.dto.ObjectType;
import auto3dtrackingsystem.repository.ClaimRepository;
import auto3dtrackingsystem.repository.UserDetailsRepository;
import auto3dtrackingsystem.repository.VacanciesRepository;
import auto3dtrackingsystem.service.ExcelService;
import auto3dtrackingsystem.util.WsSender;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.Set;
import java.util.function.BiConsumer;

@RestController
@RequestMapping("claim")
public class ClaimController {
    private final ClaimRepository claimRepository;
    private final UserDetailsRepository userDetailsRepository;
    //private final UserSkillsRepository userSkillsRepository;
    private BiConsumer<EventType, Claim> wsSender;
    private final ExcelService excelService;

    @Autowired
    private VacanciesRepository vacanciesRepository;

    @Value("${upload.path}")
    private String uploadPath;
    @Autowired
    public ClaimController(ClaimRepository claimRepository, WsSender wsSender, UserDetailsRepository userDetailsRepository, ExcelService excelService) {
        this.claimRepository = claimRepository;
        this.wsSender = wsSender.getSender(ObjectType.CLAIM, Views.IdName.class);
        this.userDetailsRepository = userDetailsRepository;
        this.excelService = excelService;
    }

    @GetMapping
    public List<Claim> list() {
        return claimRepository.findAll();
    }

    @GetMapping("{id}")
    public Claim getOne(@PathVariable("id") Claim claim) {
        return claim;
    }

    @Transactional
    @GetMapping("list/{userId}")
    public Iterable<Claim> getListForUser(@PathVariable("userId") String userId) {
        Optional<User> userOptional = userDetailsRepository.findById(userId);
        Iterable<Claim> claim = null;
        if (userOptional.isPresent()) {
            claim = claimRepository.findAllByUser(userOptional.get());
        }
        return claim;
    }

    @PostMapping
    public Claim create(@RequestBody Claim claim) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        claim.setReceiptDate(LocalDateTime.now());
        claim.setUser((User) principal);
        claim.setStatus(Status.CREATED);
        Claim updatedClaim = claimRepository.save(claim);
        wsSender.accept(EventType.CREATE, updatedClaim);
        return updatedClaim;
    }

    @GetMapping("user/role/{userId}")
    public boolean isAdmin(@PathVariable("userId") String userId) {
        Optional<User> userOptional = userDetailsRepository.findById(userId);
        Set<Role> userRole;
        boolean result = false;
        if (userOptional.isPresent()) {
            userRole = userOptional.get().getRoles();
            result = userRole.contains(Role.ADMIN);
        }
        return result;
    }

    @PostMapping("/create")
    public Claim create(@RequestBody ClaimAndUserId claimAndUserId) {
        String userId = claimAndUserId.getUserId();
        Claim claim = claimAndUserId.getClaim();
        Optional<User> userOptional = userDetailsRepository.findById(userId);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            claim.setUser(user);
        }
        claim.setReceiptDate(LocalDateTime.now());
        claim.setStatus(Status.CREATED);
        Claim updatedClaim = claimRepository.save(claim);
        wsSender.accept(EventType.CREATE, updatedClaim);
        return updatedClaim;
    }

    @PutMapping("{id}")
    public Claim update(
            @PathVariable("id") Claim claimFromDb,
            @RequestBody Claim claim
    ) {
        BeanUtils.copyProperties(claim, claimFromDb, "id");

        Claim updatedClaim = claimRepository.save(claimFromDb);
        wsSender.accept(EventType.UPDATE, updatedClaim);
        return updatedClaim;
    }

    @PutMapping("/statusChange/{id}")
    public Vacancies changeStatus(@PathVariable("id") String id, @RequestParam("status") Status status) {
        Vacancies vacancies = vacanciesRepository.findById(id).get();
        vacancies.setStatus(status);
        return vacanciesRepository.save(vacancies);
    }

    @PostMapping("/getAdmins")
    public List<String> getAdmin() {
        List<String> list = new ArrayList<>();
        for (User user : userDetailsRepository.findByRoles(Role.ADMIN)) {
            list.add(user.getName());
        }
        return list;
    }

    @PutMapping("/addChecker/{id}/{checker_id}")
    public void addChecker(@PathVariable("id") String id, @PathVariable("checker_id") String checker_id) {
        Optional<Claim> claim = claimRepository.findById(Long.parseLong(id));
        Optional<User> checker = userDetailsRepository.findById(checker_id);
        if (claim.isPresent() && checker.isPresent()) {
            claim.get().setChecker(checker.get());
            claimRepository.save(claim.get());
        }
    }

    @PutMapping("/add/{email}/{skills}")
    public void addSkills(@PathVariable("email") String email, @PathVariable("skills") String skills) {
        User user = userDetailsRepository.findByEmail(email);
        user.setSkills(skills);
        userDetailsRepository.save(user);
    }

    @PutMapping("/add/{email}")
    public void addAdmin(@PathVariable("email") String email) {
        User user = userDetailsRepository.findByEmail(email);
        user.setRoles(new HashSet<Role>() {{add(Role.ADMIN);}});
        userDetailsRepository.save(user);
    }

    @PutMapping("/add/subscriber/{createrClaim}/{claimId}/{subscriberId}")
    public void addSubscriber(@PathVariable("createrClaim") String createrClaim, @PathVariable("claimId") String claimId, @PathVariable("subscriberId") String subscriberId) {
        Claim claim = claimRepository.findById(Long.valueOf(claimId)).get();
        User user = userDetailsRepository.findById(subscriberId).get();

        Vacancies vacancies = new Vacancies();
        vacancies.setId(UUID.randomUUID().toString());
        vacancies.setClaimId(claimId);
        vacancies.setUserId(subscriberId);
        vacancies.setNameCompany(claim.getNameCompany());
        vacancies.setEmailCompany(claim.getContactEmail());
        vacancies.setDescription(claim.getDescription());
        vacancies.setEmailSubscriber(user.getEmail());
        vacancies.setCreaterClaimId(createrClaim);

        int chance = 0;
        while (chance <= 55 || chance >= 96) {
            chance = (int) (30 + Math.random()*65);
        }
        vacancies.setChance(chance + "%");

        vacanciesRepository.save(vacancies);


//        List<User> listSubscribers = claim.getSubscriber();
//        listSubscribers.add(user);
//        claim.setSubscriber(listSubscribers);
//        claimRepository.save(claim);



//        Subscribers subscribers = new Subscribers();
//        subscribers.setId(subscriberId);
//        Optional<Claim> claim = claimRepository.findById(Long.valueOf(claimId));
//        List<Subscribers> sub = claim.get().getSubscribers();
//        sub.add(subscribers);
//        claim.get().setSubscribers(sub);
//        claimRepository.save(claim.get());
    }

    @RequestMapping("/single-file")
    public Claim createFile(@RequestParam("file") MultipartFile file, @RequestParam("fileName") String fileName,
                            @RequestParam("numberOfCopy") Integer numberOfCopy, @RequestParam("claimId") String claimId) throws IOException {

        Optional<Claim> claimOptional = claimRepository.findById(Long.parseLong(claimId));
        Claim claim = null;

        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }


        if (claimOptional.isPresent()) {
            claim = claimOptional.get();
            if (claim.getFile() != null) {
                Claim newClaim = new Claim();
                BeanUtils.copyProperties(claim, newClaim, "id", "file", "fileName", "numberOfCopy");

                byte[] bytes = file.getBytes();
                newClaim.setFile(bytes);
                String uuidFile = UUID.randomUUID().toString();
                String resultFileName = uuidFile + "." + file.getOriginalFilename();
                file.transferTo(new File(resultFileName));
                newClaim.setFileName(resultFileName);
                newClaim.setNumberOfCopy(numberOfCopy);
                claimRepository.save(newClaim);
                claim = newClaim;
            } else {
                byte[] bytes = file.getBytes();
                claim.setFile(bytes);
                String uuidFile = UUID.randomUUID().toString();
                String resultFileName = uuidFile + "." + file.getOriginalFilename();
                file.transferTo(new File(uploadPath + "/" + resultFileName));
                claim.setFileName(resultFileName);
                claim.setNumberOfCopy(numberOfCopy);
                claimRepository.save(claim);
            }
        }
        return claim;
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Claim claim) {
        claimRepository.delete(claim);
        wsSender.accept(EventType.REMOVE, claim);
    }

    @GetMapping(value = "/excel", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getExcel() throws IOException {
        File excel = excelService.excel();
        InputStream in = new FileInputStream(excel);
        return IOUtils.toByteArray(in);
    }

    @GetMapping(value = "/file", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody String getFile(@RequestParam("claimId") String claimId) throws IOException {
        Optional<Claim> byId = claimRepository.findById(Long.parseLong(claimId));
        String claimFile = "";
        if (byId.isPresent()) {
            Claim claim = byId.get();
            claimFile = claim.getFileName();
        }

        return claimFile;
    }
}
