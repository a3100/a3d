package auto3dtrackingsystem.controllers;



import auto3dtrackingsystem.domain.Claim;
import auto3dtrackingsystem.domain.Role;
import auto3dtrackingsystem.domain.User;
import auto3dtrackingsystem.repository.UserDetailsRepository;
import com.google.api.client.extensions.appengine.http.UrlFetchTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


@RestController
@RequestMapping("auth")
public class AuthController {

    private final UserDetailsRepository userDetailsRepository;
    private static final JacksonFactory jacksonFactory = new JacksonFactory();

    public AuthController(UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }

    @PostMapping
    public void auth(@RequestParam("idToken") String tokenString, @RequestBody String token) throws GeneralSecurityException, IOException {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(GoogleNetHttpTransport.newTrustedTransport(), jacksonFactory)
                .setAudience(Collections.singletonList("105069402062-cgectgqm8i27trg571g78gkapsubq1cm.apps.googleusercontent.com"))
                .build();

        GoogleIdToken idToken = verifier.verify(tokenString);
        if (idToken != null) {
            GoogleIdToken.Payload payload = idToken.getPayload();
            String userId = payload.getSubject();
            User user = userDetailsRepository.findById(userId).orElseGet(() -> {
                User newUser = new User();

                newUser.setId(userId);
                newUser.setName((String) payload.get("name"));
                newUser.setEmail(payload.getEmail());
                newUser.setRoles(new HashSet<Role>() {{add(Role.USER);}});

                newUser.setLocale((String) payload.get("locale"));
                newUser.setUserpic((String) payload.get("picture"));

                return newUser;
            });
            user.setLastVisit(LocalDateTime.now());
            userDetailsRepository.save(user);
            System.out.println("User ID: " + userId);
        } else {
            System.out.println("Invalid ID token.");
        }
    }
}
