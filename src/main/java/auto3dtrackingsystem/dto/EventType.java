package auto3dtrackingsystem.dto;

public enum EventType {
    CREATE, UPDATE, REMOVE
}
